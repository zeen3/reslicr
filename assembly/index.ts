declare function apush(item: i32): i32;
declare function log(a: i32, b: i32): void;

export function mk_breaks(
  width: u32,
  height: u32,
  MAX_DIFF: i32,
  MIN_CLEAR: i32,
  MIN_BREAK: i32,
  MIN_HEIGHT_LOG2: u32,
): void {
  if (MIN_HEIGHT_LOG2 > 29) unreachable();
  // let max = width * height + image;
  let min_h = 2 << MIN_HEIGHT_LOG2;
  let clear: i32 = 0, broken: i32 = 0;
  let eq: bool = true;
  do {
    if (height <= 0) break;
    let curr = <usize>(height * width) << 2;
    let low = <usize>(--height * width) << 2;
    //log(1, curr);
    let ra = <i32>load<u8>(low), ga = <i32>load<u8>(low + 1), ba = <i32>load<u8>(low + 2)
    //log(5, ra);
    //log(6, ga);
    //log(7, ba);
    do {
      let rb = <i32>load<u8>(curr) - ra,
      gb = <i32>load<u8>(curr + 1) - ga,
      bb = <i32>load<u8>(curr + 2) - ba
      //log(11, rb);
      //log(12, gb);
      //log(13, bb);
      if (!(
        rb < MAX_DIFF && rb > -MAX_DIFF &&
        gb < MAX_DIFF && gb > -MAX_DIFF &&
        bb < MAX_DIFF && bb > -MAX_DIFF
      )) {
        eq = false;
        log(24, curr)
        break;
      }
      curr -= 4;
    } while (curr > low)
    if (eq) {
      log(30, clear)
      if (broken > MIN_BREAK) {
        log(31, clear)
        clear += 1;
        if (clear > MIN_CLEAR) {
          apush(height);
          broken = 0;
          clear = 0;
          height -= min_h;
        }
      }
    } else {
      if (clear != 0) {
        ++broken;
        clear = 0;
        log(22, broken)
      }
    }
  } while (true);
}
