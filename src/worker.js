const d = [];
const apush = d.push.bind(d)
const log = []
/* *
const exp = WebAssembly.instantiateStreaming(
  fetch('./optimized.wasm'),
  { index: { apush: d.push.bind(d), log: log.push.bind(log) } }
).then(e => e.instance.exports);
/* */
/* */
/**
 *
 * @param {ImageData} img to make breaks to
 * @param {number} maxdiff
 * @param {number} minclear
 * @param {number} minbreak
 * @param {number} minheight
 * @param {number} mindist
 * @returns {void}
 */
function mk_brk({data, height, width}, maxdiff, minclear, minbreak, minheight) {
  let broken = 0;
  let clear = 0;
  let H = (height & -(2 << minheight)) >>> 0;
  let last = height - 1;
  while (H > (1 << minheight)) {
    let eq = true
    let i = H * width * 4;
    const end = --H * width * 4 + 4;
    const R = data[end - 4], G = data[end - 3], B = data[end - 2];
    while (--i > end) {
      let b = data[--i] - B, g = data[--i] - G, r = data[--i] - R;
      if (!(
        r <= maxdiff && r >= -maxdiff &&
        g <= maxdiff && g >= -maxdiff &&
        b <= maxdiff && b >= -maxdiff
      )) {
        eq = false
        break
      }
    }
    if (eq) {
      if (broken && ++clear > minclear && broken > minbreak && (last - H) > (1 << minheight)) {
        apush(H)
        last = H
        clear = 0
        broken = 0
        H -= 1 << minheight
      }
    } else {
      ++broken
      clear = 0
    }
  }
}
/* */
/** @type {CanvasRenderingContext2D} */
let ctx;
let aborter = new AbortController();
onmessage = m => {
  switch (m.data.type) {
    case -1: aborter.abort(); aborter = new AbortController(); break
    case 0: ctx = m.data.osc.getContext('2d'); break;
    case 1:
      if (ctx.canvas.width !== m.data.width) ctx.canvas.width = m.data.width;
      if (ctx.canvas.height !== m.data.height) ctx.canvas.height = m.data.height;
      render(m.data.files, aborter.signal);
      break;
    case 2: {
      console.log('run')
      let image = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
      run({image, data: m.data})
      postMessage(d)
      d.length = 0
      break;
    }
  }
}

//let run = m => exp.then(exports => {
//  const { maxDiff = 3, minClear = 15, minBreak = 1, minHeight = 9 } = m.data;
//  const { width, height, data } = m.image;
//  let mem = exports.memory.buffer.byteLength;
//  if (mem < data.length) {
//    mem >>>= 16
//    let g = data.length >>> 16;
//    if (data.length & 0xffff) ++g
//    if (mem < g) exports.memory.grow(g - mem)
//  }
//  new Uint8Array(exports.memory.buffer).set(data);
//  exports.mk_breaks(width, height, maxDiff, minClear, minBreak, minHeight);
//  postMessage(d);
//  d.length = 0;
//  let _l = new Uint32Array(log)
//  console.log(_l);
//  log.length = 0;
//});
/**
 *
 * @param {{image: ImageData, data: { maxDiff: number, minClear: number, minBreak: number, minHeight: number } }} m
 */
function run(m) {
  const { maxDiff = 3, minClear = 15, minBreak = 1, minHeight = 7 } = m.data
  return mk_brk(m.image, maxDiff, minClear, minBreak, minHeight)
}

/**
 *
 * @param {File[]} files
 * @param {AbortSignal} signal
 */
async function render(files, signal) {
  let currentOffset = 0
  for (const file of files) {
    if (signal.aborted) {
      unblock();
      return null;
    }
    const img = await createImageBitmap(file);
    ctx.drawImage(img, 0, 0, img.width, img.height, 0, currentOffset, img.width, img.height);
    currentOffset += img.height;
    img.close();
  }
}
