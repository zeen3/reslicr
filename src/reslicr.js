const worker = new Worker('./worker.js');
worker.onmessage = console.log;
/**
 *
 * @param {string} id
 * @param {Document} node
 * @returns {HTMLElement}
 */
const $i = (id, node = document) => node.getElementById(id);

/** @type {null | HTMLCanvasElement} */
let canvas;
/** @type {HTMLTableSectionElement} */
let imgs;
///** @type {DocumentFragment} */
//let frag = document.createDocumentFragment();
/**
 * @param {number} size to human value
 * @returns {string} of the size in human values
 */
let sizeToHuman = size => {
  let i = 0;
  while (size > 1024) { ++i; size /= 1024; }
  return `${i ? size.toFixed(2) : size}\xA0${humanSizes[i]}`
}
let humanSizes = Array.from(' KMGTP', v => v===' '?'B':v+'iB')

let height = 0, width = 0, currentOffset = 0, sepHeight = 3;
let colour = '#ff69b4'
/**
 *
 * @param {Blob|File} data to read
 * @param {number?} end to read
 * @param {number?} off to read from
 * @returns {Promise<ArrayBuffer>}
 */
const read = (data, end = null, off = 0) => new Promise((r,j) => {
  let fd = new FileReader()
  fd.onload = () => r(fd.result);
  fd.onerror = () => j(fd.error);
  fd.readAsArrayBuffer(end == null ? data : data.slice(off, end));
})

let blocking = Promise.resolve(), unblock = () => {};
let block = () => {
  try { return blocking }
  finally { blocking = blocking.then(() => new Promise(r => {unblock = r})); }
}
let aborter = new AbortController(), abort = () => {
  aborter.abort();
  aborter = new AbortController();
  worker.postMessage({type: -1})
  return aborter;
}
/** @type {WeakMap<HTMLTableRowElement, File>} */
let trfi = new WeakMap();
let full = 'scale';
/** @type {HTMLTemplateElement} */
let sorter;
/** @type {File[]} */
let files = Object.seal([])

oninput = async e => {
  try {
    const {signal} = abort();
    /** @type {HTMLInputElement} */
    const t = e.target;
    switch (t.id) {
      case 'full':
      case 'fill':
      case 'scale':
      case 'none':
      if (canvas) {
        canvas.className = t.id
      }
      break;
      case 'proj': document.title = t.value.trim() ? t.value.trim() + ' - reslicr' : 'reslicr'; return;
      // case 'sepheight': await block(); if (t.value) sepHeight = t.value >>>= 0; break;
      case 'colour':
        await block();
        colour = t.value;
        document.body.attributeStyleMap.set('--colour', colour);
        break;
      case 'files':
        await block()
        files = Object.seal(Array.from(t.files))
        await renderfiles(signal)
        break;
      default:
        console.log(t);
    }
  } finally {
    unblock()
  }
}
onclick = async e => {
  try {
    const {signal} = abort();
    let t = e.target;
    if (t instanceof HTMLButtonElement) {
      if (t.dataset.sort) {
        await block()
        let file = trfi.get(t.closest('tr'));
        let idx = files.indexOf(file)
        let srt = idx;
        if (idx === -1) throw -1;
        switch (t.dataset.sort) {
          case '+1': ++srt; break;
          case '-1': --srt; break;
        }
        if (srt >= 0 && srt < files.length) {
          files[idx] = files[srt];
          files[srt] = file;
        }
        await renderfiles(signal)
      }
      if (t.id === 'run' && files.length && canvas)
        worker.postMessage({type: 2})
    }
  } finally {
    unblock()
  }
}

/**
 *
 * @param {AbortSignal} signal
 */
async function renderfiles(signal) {
  if (!imgs) imgs = $i('imgs');
  if (!sorter) sorter = $i('sorter')
  trfi = new WeakMap();
  while (imgs.rows.length) imgs.deleteRow(0);
  height = 0; width = 0; currentOffset = 0;
  for (const file of files) {
    if (signal.aborted) {
      unblock();
      return null;
    }
    /**
      * @type {null | {w: number, h: number}} is the size of an image
      */
    let size = null
    switch (file.name.slice(file.name.lastIndexOf('.') + 1)) {
      case 'jpg': case 'jpeg': {
        let b = new DataView(await read(file))
        if (!jpg.is(b)) throw new TypeError()
        size = jpg.size(b)
      }
      break
      case 'png': case 'apng': {
        let b = new DataView(await read(file, 40, 0))
        if (!png.is(b)) throw new TypeError()
        size = png.size(b)
      }
      break
      case 'webp': {
        let b = new DataView(await read(file, 32, 0))
        if (!webp.is(b)) throw new TypeError()
        size = webp.size(b)
      }
      break
      default: {
        let b = await createImageBitmap(file);
        size = {w: b.width, h: b.height};
        b.close()
      }
    }
    if (size) {
      height += size.h;
      if (width < size.w) width = size.w;
      let tr = imgs.insertRow()
      trfi.set(tr, file);
      for (let tx of [file.name, sizeToHuman(file.size), `${size.w}\xD7${size.h}`, file.type])
        tr.insertCell().innerText = tx;
      tr.insertCell().appendChild(sorter.content.cloneNode(true))
    }
  }

  if (!canvas) {
    canvas = $i('ctx');
    canvas.classList.add(full);
    let osc = canvas.transferControlToOffscreen()
    worker.postMessage({type: 0, osc}, [osc])
  }

  if (ctx.fillStyle !== colour) ctx.fillStyle = colour;

  worker.postMessage({ type: 1, files, width, height })
  canvas.attributeStyleMap.set('--height', CSS.px(height))
  canvas.attributeStyleMap.set('--width', CSS.px(width))
  //imgs.appendChild(frag);
}
/////// template stuff

/** @link {https://gitlab.com/zeen3/async-image-size/blob/master/src/png.ts} */
const png = {
  // 0..8 === '\x89png\r\n\x1a\n'
  is: b => b.getUint32(0) === 0x89504e47
    && b.getUint32(4) === 0x0d0a1a0a
    // 12..16 === 'ihdr'
    // ? true
    // : 12..16 === 'cgbi' && 28..32 === 'ihdr'
    && (
      b.getUint32(12) === 0x49484452
      ? true
      : b.getUint32(12) === 0x43674249
      && b.getUint32(28) === 0x49484452
    ),
  /**
   * @param {DataView} buf to read from
   * @returns {{w: number, h: number}}
   */
  size: buf => buf.getUint32(12) !== 0x43674249
    ? {w: buf.getUint32(16), h: buf.getUint32(20)}
    : {w: buf.getUint32(32), h: buf.getUint32(36)}
}
/** @link {https://gitlab.com/zeen3/async-image-size/blob/master/src/jpg.ts} */
const jpg = {
  is(buf) {return buf.getUint16(0) === 0xFF_D8},
  /**
   * @param {DataView} buf to read from
   * @returns {{w: number, h: number}}
   */
  size(buf) {
    let offset = 4
    while (offset < buf.byteLength) {
      let i = buf.getUint16(offset)
      offset += i
      console.assert(0xff === buf.getUint8(offset), 'jpeg ok')
      let next = buf.getUint8(offset + 1)
      switch (next) {
        case 0xC0: case 0xC1: case 0xC2:
          return {
            h: buf.getUint16(offset + 5),
            w: buf.getUint16(offset + 7)
          }
        default:
      }
      offset += 2
    }

    throw new TypeError('invalid jpeg, no size retrievable')
  }
}
/** @link {https://gitlab.com/zeen3/async-image-size/blob/master/src/webp.ts} */
const webp = {
  // 0..3 === RIFF
  // 8..12 === WEBP
  // 12..14 === VP
  // 15 === 8
  is: (b) => b.getUint32(0) === 0x52_49_46_46
    && b.getUint32(8) === 0x57_45_42_50
    && b.getUint16(12) === 0x56_50
    && b.getUint8(15) === 0x38,
  /**
   * @param {DataView} b to read from
   * @returns {{w: number, h: number}}
   */
  size(b) {
    switch (b.getUint32(12)) {
      case 0x56_50_38_58:
        if (b.getUint8(20) & 0xc1)
          throw new TypeError('invalid webp, no size retrievable')
        else return {
          w: 1 + ((b.getUint16(25, true) << 8) |
            b.getUint8(24)),
          h: 1 + ((b.getUint16(28, true) << 8) |
            b.getUint8(27))
        }
        // ... yeah uh okay
      case 0x56_50_38_20:
        if (b.getUint8(20) === 0x2F)
          throw new TypeError('invalid webp, no size retrievable')
        else return {
          w: b.getUint16(26, true) & 0x3FFF,
          h: b.getUint16(28, true) & 0x3FFF
        }
        // wut
      case 0x56_50_38_4C:
        if ((b.getUint32(22) & 0xffffff) === 0x9D012A)
          throw new TypeError('invalid webp, no size retrievable')
        else return {
          w: 1 + (((b.getUint8(22) & 0x3F) << 8) |
            b.getUint8(21)),
          h: 1 + ((((b.getUint8(24) & 0xF) << 10)) |
            (b.getUint8(23) << 2) |
            ((b.getUint8(22) & 0xC0) >>> 6))
        }
        // k srs wtf here
      default:
        throw new TypeError('invalid webp, no size retrievable')
    }
  }
}

try {
  for (const [name, syntax = '<length>', initialValue = CSS.px(360)] of [
    ['--maxwidth'], ['--height'], ['--width'],
    ['--colour', '<color>', 'hotpink']
  ]) CSS.registerProperty({ name, syntax, inherits: true, initialValue })
} catch (e) {}
